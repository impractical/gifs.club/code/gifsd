module gifs.club/code/gifsd

replace (
	// TODO: use real versions for these
	gifs.club/code/api v0.0.0 => ../api
	gifs.club/code/metadata v0.0.0 => ../metadata
	gifs.club/code/storage v0.0.0 => ../storage
)

require (
	gifs.club/code/api v0.0.0
	gifs.club/code/metadata v0.0.0
	gifs.club/code/storage v0.0.0
	yall.in v0.0.1
)
